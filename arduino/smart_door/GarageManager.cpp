//Made by Neri Luca
#include "Arduino.h"
#include <Servo.h>
#include "GarageManager.h"
#define SERVO_INCREMENT 2

//shared variables
int presenceDetected; 
extern bool userLogged;
extern bool closeSession;
extern bool forceClose;
extern bool insideRoom;
int pos = 10;
bool isDoorOpen = false;
bool printed = false;
int counter = 0;
bool servoAttached = false;
Servo myServo;
//class constructor with hw pins, calls the base constructor
MotionSensor::MotionSensor( int servoPin, int sensorPin, int closePin): MotionSensor(sensorPin) {
 
  this->buttonPin = closePin;
  this->servoP = servoPin;
  pinMode(this->buttonPin,INPUT);
  
  myServo.write(pos);
  
}

MotionSensor::MotionSensor(int pin) {
  //sets the variables
  this->sensorPin = pin;
  firstTime = false;
  pinMode(pin, INPUT);
  state = WAIT_LOG; //setup first state

}

//method inherited from the task class, setups the period
void MotionSensor::init(int period) {
  Task::init(period);
  state = WAIT_LOG;
}

//method that is called every "period" ms
void MotionSensor::tick() {
  //do some operations base on the current task state
  switch (state) {
    
    case WAIT_LOG: {
        
        //if(userLogged && !isDoorOpen){
        if(userLogged){ 
          if(!servoAttached){
          myServo.attach(this->servoP);
          servoAttached = true;
        }   
        if(pos >= 190){
            pos = 190;
            myServo.write(pos);
            Serial.println("o");
            myServo.detach();
            servoAttached = false;
            isDoorOpen=true;
            state = WAIT_PRESENCE;
          }else{
            pos = pos + SERVO_INCREMENT;
            myServo.write(pos);
          }
        }
        break;
      }
    //case which simulates the grage isDoorOpening
    case WAIT_PRESENCE: {
        presenceDetected = digitalRead(this->sensorPin);//read value from the sensor
        if(presenceDetected != HIGH && counter < 200){
          counter++;
          if(counter == 150){
            //must close session on mobile 
            closeSession = true;
            //must send log to raspi
            Serial.println("t");
            counter = 0;
            state=CLOSING;
          }
        }else if(presenceDetected == HIGH){
          counter = 0;
          //send log to raspberry
          insideRoom = true;
          Serial.println("i");
          state = WAIT_CLOSURE;
        }
        break;

      }
    //read the button state
    case WAIT_CLOSURE: {

        int value = digitalRead(this->buttonPin);
        if(value == HIGH || forceClose){
          closeSession = true;
          forceClose=false;
          state = CLOSING;
        }else{
          closeSession=false;
        }
        break;
      }
      case CLOSING: {
        
         if(!printed){
          Serial.println("c");
          printed = true;
         }
        if(!servoAttached){
          myServo.attach(this->servoP);
          servoAttached = true;
        }
          if(pos <= 10){
            
            pos=10;
            myServo.write(pos);
            myServo.detach();
            servoAttached = false;
            insideRoom = false;
            isDoorOpen=false;
            userLogged=false;
            closeSession = false;
            state = WAIT_LOG;
            printed = false;
          }else{
            pos = pos - SERVO_INCREMENT;
        
          myServo.write(pos);
          }
        break;
      }
  }


}






