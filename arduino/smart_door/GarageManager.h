//Made by Neri Luca
#ifndef __MOTIONSENS__
#define __MOTIONSENS__


#include "Task.h"
class MotionSensor: public Task {



  public:
    MotionSensor(int pin);
    MotionSensor(int ledPin, int closePin, int sensorPin);
    enum {CLOSING, WAIT_CLOSURE, WAIT_PRESENCE, WAIT_LOG} state; //enum which contains the task states
    void isApproching();
    void init(int period);
    void tick();
  private:
    int ledPin;
    int sensorPin;
    int buttonPin,servoP;
    bool firstTime;
    void resetInputs();
    bool readValue();
    int fadeIntensity = 0;
    char data = ' '; 
    bool stopMessageDisplayed = false; 
    int timeoutCounter = 0;


};

#endif
