//Made by Neri Luca
#include "Arduino.h"
#include "RadarTask.h"
#include "MsgService.h"
#include "SoftwareSerial.h"
#include <Wire.h>
#define MIN_DIST 0.5
#define MIN_SEC 10

 float currentDistance;
extern bool userLogged;
extern bool closeSession;
extern bool forceClose;
extern bool insideRoom;
int tempCounter = 0; //current servo position


int currentIntensity = 0;
int oldIntensity = 0;
int fadeAmount = 5;
int temperature = 21;
int old_temp = 0;
MsgService msgService(2,3);
//class constructor with hw pins
RadarTask::RadarTask(int sonarPin, int trigPin,int fadeLed) {
  //hardware parameters initialization
  this->sonarPin = sonarPin;
  this->trigPin = trigPin;
  this->fadePin = fadeLed;
  pinMode(fadePin, OUTPUT);
  analogWrite(this->fadePin,0);
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
  pinMode(this->sonarPin, INPUT);
  pinMode(this->trigPin, OUTPUT);
  msgService.init();  
  

}
//method used to get the distance
void RadarTask::getDistance() {
  digitalWrite(this->trigPin, LOW);
  delayMicroseconds(3);
  digitalWrite(this->trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(this->trigPin, LOW);

  float tUS = pulseIn(this->sonarPin, HIGH);
  float t = tUS / 1000.0 / 1000.0 / 2;
  currentDistance = t * vs;//converting the distance(meters) to centemeters
  
}
//method inherited from the task class, setups the period
void RadarTask::init(int period) {
  Task::init(period);
  //state = WAIT_DISTANCE;
  state = WAIT_PAIRING;
}

//method that is called every "period" ms
void RadarTask::tick() {
  //do some operations based on the current task state
  switch (state) {

    case WAIT_PAIRING: {
      
        if (msgService.isMsgAvailable()) {
            
            Msg* message = msgService.receiveMsg();
            String msg = message->getContent();
            if(msg =="p"){
              state=WAIT_DISTANCE;
              Serial.println("paired");
            }
            
            delete message;
          }
         
        break;
      }

    case WAIT_DISTANCE: {
        getDistance();
        if(currentDistance < MIN_DIST){
          //Serial.println(currentDistance);
          tempCounter++;
          if(tempCounter >= 70){ //must send message to android and wait for credentials
            msgService.sendMsg(Msg("benvenuto"));  
            state = WAIT_CREDENTIALS;
          }
        }
        break;
      }
    
    case WAIT_CREDENTIALS: {
          if (msgService.isMsgAvailable()) {
            
            Msg* message = msgService.receiveMsg();
            String msg = message->getContent();
            Serial.println(msg);//send credentials to Raspi
            state = WAIT_VERDICT;
            delete message;
          }
         
        break;

      }
      //the servo must block because an object is being tracked
    case WAIT_VERDICT: {
        
        char data;
        //bool verdict = true;
        if (Serial.available()>0) {
          data = Serial.read();
          
        }
        
        if(data == 'P'){ //user logged succesfully
        //if(verdict){
          msgService.sendMsg(Msg("logged"));
          //must send data to the RASPI <---no need to do this, raspi already checked
          userLogged=true;
          state=WAIT_INSIDE;
        }else{
          msgService.sendMsg(Msg("errore login"));  
          state = WAIT_CREDENTIALS;
          //send data to raspi & led FLASH <---no need to do this, raspi already checked
        }
        break;
      }
      case WAIT_INSIDE:{
        if(insideRoom){
          msgService.sendMsg(Msg("i")); 
           state=RECEIVING_INPUTS;
        }else if(closeSession){
          msgService.sendMsg(Msg("c")); 
           state=WAIT_DISTANCE; 
           //state=WAIT_PAIRING;
           tempCounter=0;
        }

        break;
      }

    //system receive datas from android
    case RECEIVING_INPUTS: {
        //TODO: send temp value;
        //TODO: check button pressed to close session---done
        
        if(old_temp != temperature || oldIntensity != currentIntensity){
          old_temp = temperature;
          String message = String(old_temp);
          if(oldIntensity != currentIntensity){
            oldIntensity = currentIntensity;
          }
          msgService.sendMsg(Msg(message));
          Serial.print(old_temp);Serial.print("-");Serial.println(oldIntensity);
        }
        
        
        if (msgService.isMsgAvailable()) {
            Msg* message = msgService.receiveMsg();
            String msg = message->getContent();
            temperature = 55;
            //Serial.println(msg);
            if(msg == "+"){ //increase intensity
              
              currentIntensity += fadeAmount;
              if(currentIntensity >=255){
                currentIntensity = 255;
              }
              analogWrite(this->fadePin,currentIntensity);
              
            }else if(msg == "-"){//decrease intensity
              temperature = 8;
              currentIntensity -= fadeAmount;
              if(currentIntensity <=0){
                currentIntensity = 0;
              }
              analogWrite(this->fadePin,currentIntensity);
              
            }else if(msg == "c"){ //close session
              //userLogged=false;
              forceClose = true;
              /*closeSession=false;
              insideRoom = false;
              tempCounter=0;
              state = WAIT_DISTANCE;*/
            }
          }
          if(closeSession){
            //analogWrite(this->fadePin,0);
            msgService.sendMsg(Msg("c"));
            userLogged=false;
            closeSession=false;
            insideRoom = false;
            forceClose = false;
            tempCounter=0;
            state = WAIT_DISTANCE;
            
          }

        
        break;

      }


  }


}







