//Made by Neri Luca
#ifndef __RADARTASK__
#define __RADARTASK__
#include "Task.h"
class RadarTask: public Task {



  public:
    RadarTask( int sonarPin, int trigPin, int fadeLed);
    enum {WAIT_INSIDE,RECEIVING_INPUTS, WAIT_DISTANCE, WAIT_CREDENTIALS, WAIT_VERDICT, WAIT_PAIRING} state,previousState; //enum which contains the task states   
    void init(int period);
    void tick();
    void getDistance();
  private:
    float currentDistance = 0;
    const float vs = 331.5 + 0.6 * 20;
    int fadePin;
    int servo;
    int sonarPin, trigPin;
    char data=' ';
    void readFromSerial();
};

#endif
