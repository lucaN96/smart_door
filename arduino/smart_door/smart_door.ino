#include "Scheduler.h"
#include "GarageManager.h"
#include "RadarTask.h"

/*
 *  BT module connection:  
 *  - pin 2 <=> TXD
 *  - pin 3 <=> RXD
 *
 */ 
bool closeSession =false;
bool userLogged=false;
bool forceClose = false;
bool insideRoom=false;
Scheduler sched;
void setup() {
  sched.init(20);
  Serial.begin(9600);
  
  //echo, tring, fade led pwm
  Task* t1 = new RadarTask(4,5,6);
  t1->init(20);
  sched.addTask(t1);

  //servo, pir, button
  Task* t2 = new MotionSensor(7,8,10);
  t2->init(50);
  sched.addTask(t2);

 
  
}

void loop() {
  sched.schedule();//scheduler in action
}

