package log_service;
import java.io.*;
import java.net.*;

public class JavaClient {
	public JavaClient()  throws Exception{
		boolean exit=false;
		 String sentence;//stringa che viene letta da tastiera
		 String modifiedSentence;//stringa letta in risposta dal server
		 //input stream che ottiene la stringa digitata dall'utente tramite la tastiera(System.in)
		 BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		 //apertura socket per la connessione per l'host localhost e per la porta 6789
		 Socket clientSocket=null;
		
		 while(true){ 
		   clientSocket= new Socket("localhost", 6789);
		   System.out.println(clientSocket.getLocalPort());
		 DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());//apre lo stream verso il server per far uscire il messaggio
		 BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));//stream in entrata dal server
				
				
					System.out.println("Insert LOG or STATUS:");
			 		
				 	 sentence = inFromUser.readLine();//lettura input da tastiera
					if(sentence.equals("quit"))
					{
						exit=true;

					}
					
				 outToServer.writeBytes(sentence + '\n');//scrive sulla socket il messaggio
				 
				 modifiedSentence = inFromServer.readLine();//legge il messaggio di risposta dal server 
				 
				System.out.println("FROM SERVER: " + modifiedSentence);
				clientSocket.close();//chiude la connessione e la socket, quando l'utente digita quit
		}
		
	}
	public static void main(String argv[]) throws Exception
	 {
		
		JavaClient c1 = new JavaClient();
		
	 }
}
