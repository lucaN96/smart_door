package log_service;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class Test {
    private static File file = new File(System.getProperty("user.dir")+"/accessLogFile.txt"); //log file
    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(6789), 0);
        server.createContext("/logAccess", new MyHandler());
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    static class MyHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "This is the response";
            //t.sendResponseHeaders(200, response.length());
            
            OutputStream os = t.getResponseBody();
            //os.write(response.getBytes());
            String readedString = "";
            String temp = "";
            String[] vett = null;
            int maxLength = 0;
            String separator = "\r\n";
            
           
            
                //t.sendResponseHeaders(200, maxHttpHeaderSize);
                try (BufferedReader bfw = new BufferedReader(new FileReader(file))) {
                    
                    while ((readedString = bfw.readLine()) != null && readedString.length() >= 1) {
                        
                        vett = readedString.split(" ");
                        if (vett[0].equals("At")) {
                            
                            os.write(readedString.getBytes());
                            os.write(separator.getBytes());
                            
                        }else if(vett[0].equals("--TEMP")){
                           temp = readedString;
                        }

                    }
                    os.write(separator.getBytes());
                    os.write(temp.getBytes());
                    
                }catch (final FileNotFoundException e) {

                    e.printStackTrace();
                }catch (final IOException e) {

                    e.printStackTrace();
                }
            
            os.close();
        }
    }

}