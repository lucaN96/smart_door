package smart_door;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Calendar;
import java.util.Enumeration;

import smart_door.Led;
import smart_door.SerialMonitor;
import gnu.io.CommPortIdentifier;
/**
 * 
 * Class made by Neri Luca 0000758738
 *
 *
 */
public class Gateway extends Thread  {
    
    SerialMonitor sr;
    private Led ledInside;
    private Led ledFailedAccess;
    String readedString = "";
    String previousReceivedString = "";
    String[] usernames = {"luca","matteo","andrea"};
    String[] passwords =  {"1","2","3"};
    String username;
    boolean printed = false;
    boolean logged = false;
    boolean swapped = false;
    //private File file = new File(System.getProperty("user.dir")+"/accessLogFile.txt"); //log file
    private File file = new File("/var/www/html/accessLogFile.txt"); //log file
    public Gateway(){
        
        ledInside = new Led(0);
        ledFailedAccess = new Led(2);
        sr = new SerialMonitor();
        
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
        CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
        //currPortId = CommPortIdentifier.getPortIdentifier("/dev/ttyACM0"); //selecting port
        
        //starting the serial monitor, with the given parameters (port name, baud rate)
        sr.start(currPortId.getName(), 9600); 
        this.run();
    }
    public void run(){
        boolean fileModified = false;
        while(true){
         
        Calendar cal = Calendar.getInstance(); //used to print the current time
        readedString = sr.returnDatas();
        sr.resetData();
        
        if(readedString.equals("paired") && !printed){
            System.out.println("BT and DOOR are now connected");
            printed = true;
            
        }else if(readedString.contains(" ") && !logged){
            
            String[] vett = readedString.split(" ");
            
            String u = vett[0];
            username = u;
            String p = vett[1];
            
            for(int i =0; i< usernames.length;i++){
                if(usernames[i].equals(u) && passwords[i].equals(p)){
                    //log In successful
                    
                    sr.sendDatas("P");
                    logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+" :User :"+u+" :LOGGED ");
                    logged = true;
                    fileModified = true;
                }
            }
            if(logged == false && !readedString.isEmpty()){
                sr.sendDatas("N");
                logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":User :"+u+" :Not authorized");
                fileModified = true;
                try {
                    ledFailedAccess.switchOn();
                    sleep(100);
                    ledFailedAccess.switchOff();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
            }
        }else if(readedString.equals("i") && logged){
            logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":User :"+username+" :Now inside" );
            fileModified = true;
            try {
                ledInside.switchOn();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }else if(readedString.equals("t")){
            logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":User :"+username+":Open but not entered");
            logged = false;
            fileModified = true;
            try {
                ledFailedAccess.switchOn();
                sleep(100);
                ledFailedAccess.switchOff();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }else if(readedString.equals("c") && logged){
            logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":User:"+username+" :Has left the room");
            logged=false;
            fileModified = true;
            try {
                ledInside.switchOff();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else if(readedString.contains("-") && logged){
            
                String[] vett = readedString.split("-");
            
            String temperature = vett[0];
            String intensity = vett[1];
            logger("TEMP "+temperature+" Intensity "+intensity + " (Time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+")");
            fileModified = true;
        }
        else if(readedString.equals("o")){
            System.out.println(" DOOR OPEN, waiting presence");
            fileModified = true;
        }
        
        /*if(fileModified){
            requestLogToService();
            fileModified=false;
        }*/
        }
        
    }
    /**Method used to display and log on a file some messages
     * 
     * @param message info to display and save
     */
    public void logger(final String message) {
        try (BufferedWriter bw= new BufferedWriter(new FileWriter(file, true))) {
          bw.write(message+"\n"); //write on the file          
          System.out.println(message); //display in the console
        } catch (IOException exception) {
          System.out.println(exception.getMessage());
          exception.printStackTrace();
        }
      }
    /*private void requestLogToService(){
       
        try {
        String sentence;//stringa che viene letta da tastiera
        String modifiedSentence;//stringa letta in risposta dal server
       
        //apertura socket per la connessione per l'host localhost e per la porta 6789
        Socket clientSocket = new Socket("localhost", 6789);
       
        
      DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());//apre lo stream verso il server per far uscire il messaggio
      BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));//stream in entrata dal server
                     
                     
                             
                             
                      sentence = "LOG";//lettura input da tastiera
                             
                             
                      outToServer.writeBytes(sentence + '\n');//scrive sulla socket il messaggio
                      
                      
                     clientSocket.close();//chiude la connessione e la socket, quando l'utente digita quit
    }catch(Exception ex){
        System.out.println(ex.getMessage());
    }
    }*/


}
