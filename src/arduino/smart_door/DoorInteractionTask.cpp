//Made by Neri Luca
#include "Arduino.h"
#include "DoorInteractionTask.h"
#include "MsgService.h"
#include "SoftwareSerial.h"
#include <Wire.h>
#define MIN_DIST 0.5
//#define MIN_SEC 10

 //float currentDistance;
extern bool userLogged;
extern bool closeSession;
extern bool forceClose;
extern bool insideRoom;

MsgService msgService(2,3); // uses software serial with 2&3 pins to comunicate
//class constructor with hw pins
DoorInteractionTask::DoorInteractionTask(int sonarPin, int trigPin,int fadeLed) {
  //hardware parameters initialization
  this->sonarPin = sonarPin;
  this->trigPin = trigPin;
  this->fadePin = fadeLed;
  pinMode(fadePin, OUTPUT);
  analogWrite(this->fadePin,0);
  pinMode(this->sonarPin, INPUT);
  pinMode(this->trigPin, OUTPUT);
  msgService.init();  
  

}
//method used to get the distance
void DoorInteractionTask::getDistance() {
  digitalWrite(this->trigPin, LOW);
  delayMicroseconds(3);
  digitalWrite(this->trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(this->trigPin, LOW);

  float tUS = pulseIn(this->sonarPin, HIGH);
  float t = tUS / 1000.0 / 1000.0 / 2;
  currentDistance = t * vs;//converting the distance(meters) to centemeters
  
}
//method inherited from the task class, setups the period
void DoorInteractionTask::init(int period) {
  Task::init(period);
  temp = new TempSensor();
  state = WAIT_PAIRING;
}

//method that is called every "period" ms
void DoorInteractionTask::tick() {
  //do some operations based on the current task state
  switch (state) {

    //waits door & bluetooth pairing
    case WAIT_PAIRING: {
      
        if (msgService.isMsgAvailable()) {
            
            Msg* message = msgService.receiveMsg();
            String msg = message->getContent();
            if(msg =="p"){
              state=WAIT_DISTANCE;
              Serial.println("paired"); //message sent ro Gateway/raspi
            }
            
            delete message;
          }
         
        break;
      }
    //checks if user is near the door
    case WAIT_DISTANCE: {
        getDistance();
        if(currentDistance < MIN_DIST && !closeSession){
          
          tempCounter++;
          if(tempCounter >= 50){ 
            msgService.sendMsg(Msg("BENVENUTO"));  //message sent to Android application
            state = WAIT_CREDENTIALS;
          }
        }
        break;
      }
    
    case WAIT_CREDENTIALS: {
          if (msgService.isMsgAvailable()) {
            
            Msg* message = msgService.receiveMsg();
            String msg = message->getContent(); //read message from the channel
            Serial.println(msg);//send credentials to Raspi
            state = WAIT_VERDICT;
            delete message;
          }
         
        break;

      }
    //case used to wait the login result
    case WAIT_VERDICT: {
        
        char data;
        
        if (Serial.available()>0) { //message received from the gateway
          data = Serial.read();
          
        }
        
        if(data == 'P'){ //user logged succesfully
        
          userLogged=true;
          delay(50);
          state=WAIT_INSIDE;
          
        }else{
          msgService.sendMsg(Msg("errore login"));  
          state = WAIT_CREDENTIALS;
          
        }
        break;
      }
      
      case WAIT_INSIDE:{
        if(insideRoom){
          
          msgService.sendMsg(Msg("i"));//user inside room 
           state=RECEIVING_INPUTS;
        }else if(closeSession){
          msgService.sendMsg(Msg("c")); 
           state=WAIT_DISTANCE; 
           
           tempCounter=0;
        }

        break;
      }

    //system receive datas from android
    case RECEIVING_INPUTS: {
        //read temperature value from sensor
        temperature = temp->readTemperature();
        if(old_temp != temperature || oldIntensity != currentIntensity){
          old_temp = temperature;
          String message = String(old_temp);
          if(oldIntensity != currentIntensity){
            oldIntensity = currentIntensity;
          }
          //send current temp&intensity values to Android and Raspi
          msgService.sendMsg(Msg(message));
          Serial.print(old_temp);Serial.print("-");Serial.println(oldIntensity);
        }
        
        //message received from the app
        if (msgService.isMsgAvailable()) {
            Msg* message = msgService.receiveMsg();
            String msg = message->getContent();
            temperature = 55;
            //user want to increase led brightness
            if(msg == "+"){ //increase intensity
              
              currentIntensity += fadeAmount;
              if(currentIntensity >=255){
                currentIntensity = 255;
              }
              analogWrite(this->fadePin,currentIntensity);
              
            }else if(msg == "-"){ //decrease intensity
              temperature = 8;
              currentIntensity -= fadeAmount;
              if(currentIntensity <=0){
                currentIntensity = 0;
              }
              analogWrite(this->fadePin,currentIntensity);
              
            }else if(msg == "c"){ //close button pressed on the app
              forceClose = true;
              
            }
          }
          //session closed, must send "close" to the app and reset inputs
          if(closeSession){
            
            msgService.sendMsg(Msg("c"));
            userLogged=false;
            closeSession=false;
            insideRoom = false;
            forceClose = false;
            tempCounter=0;
            state = WAIT_DISTANCE;  //return to "IDLEE" state
          }  
        break;
      }
  }
}







