//Made by Neri Luca
#ifndef __DOORINTERACTIONTASK__
#define __DOORINTERACTIONTASK__
#include "Task.h"
#include "TempSensor.h"
class DoorInteractionTask: public Task {



  public:
    DoorInteractionTask( int sonarPin, int trigPin, int fadeLed);
    enum {WAIT_INSIDE,RECEIVING_INPUTS, WAIT_DISTANCE, WAIT_CREDENTIALS, WAIT_VERDICT, WAIT_PAIRING} state; //enum which contains the task states   
    void init(int period);
    void tick();
    void getDistance();
    
  private:
    float currentDistance = 0;
    const float vs = 331.5 + 0.6 * 20;
    int fadePin;
    int servo;
    int sonarPin, trigPin;
    char data=' ';
    TempSensor* temp;
    int tempCounter = 0; 
    int currentIntensity = 0;
    int oldIntensity = 0;
    int fadeAmount = 5;
    int temperature = 21;
    int old_temp = 0;
};

#endif
