//Made by Neri Luca
#include "Arduino.h"
#include <Servo.h>
//#include "PWMServo.h"
#include "DoorManagmentTask.h"
#define SERVO_INCREMENT 2

//shared variables

extern bool userLogged;
extern bool closeSession;
extern bool forceClose;
extern bool insideRoom;
Servo myServo;
//class constructor with hw pins, calls the base constructor
DoorManagmentTask::DoorManagmentTask( int servoPin, int sensorPin, int closePin){
 
  this->buttonPin = closePin;
  this->servoP = servoPin;
  
  this->sensorPin = sensorPin;
  
  pinMode(this->sensorPin, INPUT);
  pinMode(this->buttonPin,INPUT);
  state = OPENING; //setup first state
  
  
}



//method inherited from the task class, setups the period
void DoorManagmentTask::init(int period) {
  Task::init(period);
  state = OPENING;
}

//method that is called every "period" ms
void DoorManagmentTask::tick() {
  //do some operations base on the current task state
  switch (state) {

    
    case OPENING: {
        
        //waits log
        if(userLogged){
          myServo.attach(5);
          
          if(pos >= 190){
              pos = 190;
             //myServo.attach(5);
              myServo.write(pos);
              delay(15);
              Serial.println("o");
              //myServo.detach();
              myServo.detach();
              //servoAttached = false;
              //isDoorOpen=true;
              state = WAIT_PRESENCE;
            }else{// user logged, must open door
              delay(10);
              pos = pos + SERVO_INCREMENT;
              //myServo.attach(5);
              myServo.write(pos);
              delay(15);
              myServo.detach();
            }
        } 
        break;
      }
    //waiting user presence inside room
    case WAIT_PRESENCE: {
        presenceDetected = digitalRead(this->sensorPin);//read value from the sensor
        if(presenceDetected != HIGH && counter < 60){
          counter++;
          if(counter == 50){
            //must close session on mobile 
            closeSession = true;
            //must send log to raspi
            Serial.println("t");//timeout
            counter = 0;
            state=CLOSING;
          }
          //user inside the room
        }else if(presenceDetected == HIGH){
          counter = 0;
          //send log to raspberry
          insideRoom = true;
          Serial.println("i");
          state = WAIT_CLOSURE;
        }
        break;

      }
    //read the button state
    case WAIT_CLOSURE: {

        int value = digitalRead(this->buttonPin);
        //checks if the button is pressed or if the user closed the session from mobile
        if(value == HIGH || forceClose){
          closeSession = true;
          forceClose=false;
          state = CLOSING;
        }else{
          closeSession=false;
        }
        break;
      }
     //end-case
     case CLOSING: {
        
         if(!printed){
          delay(15);
          Serial.println("c");
          printed = true;
          myServo.attach(5);
          
         }
       
          if(pos <= 10){
            
            pos=10;
           
            myServo.write(pos);
            delay(15);
            myServo.detach();
            //new inputs
            
            insideRoom = false;
            
            userLogged=false;
            closeSession = false;
            state = OPENING; //return to initial state
            printed = false;
          }else{ //simulates door closing
            pos = pos - SERVO_INCREMENT;
          
          myServo.write(pos);
          delay(15);
         
          }
        break;
      }
  }


}






