//Made by Neri Luca
#ifndef __DOORMANAGMENTTASK__
#define __DOORMANAGMENTTASK__


#include "Task.h"
class DoorManagmentTask: public Task {



  public:
    DoorManagmentTask(int ledPin, int closePin, int sensorPin);//constructor
    enum {OPENING,CLOSING, WAIT_CLOSURE, WAIT_PRESENCE} state; //enum which contains the task states
    
    void init(int period);
    void tick();
  private:
    int ledPin;
    int sensorPin;
    int buttonPin,servoP;
    char data = ' '; 
    int presenceDetected; 
    int pos = 10;
    bool printed = false;
    int counter = 0;
    


};

#endif
