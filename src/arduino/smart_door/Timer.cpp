
#include "Arduino.h"
#include "Timer.h"
#include "FlexiTimer2.h"//needed to prevent conficts between servo's timer and scheduler's timer
volatile bool timerFlag;

void method() {
  timerFlag = true;
  
}

Timer::Timer() {
  timerFlag = false;
}

/* period in ms */
void Timer::setupPeriod(int period) {
  //code used to set the period and the method to call
 FlexiTimer2::set(period,method);
 FlexiTimer2::start();

}

void Timer::waitForNextTick() {
  /* wait for timer signal */
  while (!timerFlag) {}
  timerFlag = false;

}
