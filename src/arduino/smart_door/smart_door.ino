#include "Scheduler.h"
#include "DoorInteractionTask.h"
#include "DoorManagmentTask.h"

#define LEDON 13
/*
 *  BT module connection:  
 *  - pin 2 <=> TXD
 *  - pin 3 <=> RXD
 *
 */ 
bool closeSession =false;
bool userLogged=false;
bool forceClose = false;
bool insideRoom=false;
Scheduler sched;
void setup() {
  
  
  Serial.begin(9600);
  
  //System started--> ledOn switched
  pinMode(LEDON,OUTPUT);
  digitalWrite(LEDON,HIGH);
 
  sched.init(60);
  
  //echo, trig, fade led pwm pins
  Task* t1 = new DoorInteractionTask(7,8,6);
  t1->init(60);
  sched.addTask(t1);

  //servo, pir, close-button pins 
  Task* t2 = new DoorManagmentTask(5,12,11);
  t2->init(60);
  sched.addTask(t2);
}

void loop() {
  sched.schedule();//scheduler in action
}

