package smart_door;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Calendar;
import java.util.Enumeration;

import smart_door.Led;
import smart_door.SerialMonitor;
import gnu.io.CommPortIdentifier;
/**
 * 
 * Class made by Neri Luca 0000758738
 *
 *
 */
public class Gateway extends Thread  {
    
    SerialMonitor sr;
    private Led ledInside;
    private Led ledFailedAccess;
    String readedString = "";
    String previousReceivedString = "";
    String[] usernames = {"luca","matteo","andrea"};
    String[] passwords =  {"1","2","3"};
    String username;
    boolean printed = false;
    boolean logged = false;
    boolean swapped = false;
    
    private File file = new File("/var/www/html/accessLogFile.txt"); //log file
    public Gateway(){
        
        ledInside = new Led(0);
        ledFailedAccess = new Led(2);
        sr = new SerialMonitor();
        
        Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
        CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
        
        //if file already exists in /var/www/html --> delete it and create a new one
        if(file.exists()){
            file.delete();
            file = new File("/var/www/html/accessLogFile.txt");
        }
        
        //starting the serial monitor, with the given parameters (port name, baud rate)
        sr.start(currPortId.getName(), 9600); 
        this.run();
    }
    public void run(){
        
        while(true){
         
        Calendar cal = Calendar.getInstance(); //used to print the current time
        readedString = sr.returnDatas();
        sr.resetData();
        
        //bluetooth and door are connected, door sent message to the gateway
        if(readedString.equals("paired") && !printed){
            System.out.println("BT and DOOR are now connected");
            printed = true;
            
        }else if(readedString.contains(" ") && !logged){//username and pasword received
            
            String[] vett = readedString.split(" ");
            
            String u = vett[0];
            username = u;
            String p = vett[1];
            
            //check if the user is registered on the gateway
            for(int i =0; i< usernames.length;i++){
                if(usernames[i].equals(u) && passwords[i].equals(p)){
                    //log In successful
                    
                    sr.sendDatas("P");
                    logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+" :User :"+u+" :LOGGED ");
                    logged = true;
                    
                }
            }
            //user isn't enabled to access the door
            if(logged == false && !readedString.isEmpty()){
                sr.sendDatas("N");
                logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":User :"+u+" :Not authorized");
                
                try {
                    //ledFailed must blink
                    ledFailedAccess.switchOn();
                    sleep(100);
                    ledFailedAccess.switchOff();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
            }
            
        }else if(readedString.equals("i") && logged){ //user is inside the room, Arduino sent msg "i"
            
            logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":User :"+username+" :Now inside" );
            
            try {
                ledInside.switchOn();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }else if(readedString.equals("t")){ //door is open but user didn't enter in time
            logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":User :"+username+":Open but not entered");
            logged = false;
            
            try {
                ledFailedAccess.switchOn();
                sleep(100);
                ledFailedAccess.switchOff();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
        }else if(readedString.equals("c") && logged){ //user clicked the closeButton on the app or on the door
            logger("At time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+":User:"+username+" :Has left the room");
            logged=false;
            
            try {
                ledInside.switchOff();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else if(readedString.contains("-") && logged){ //temperature & brightness values received from arduino
            
                String[] vett = readedString.split("-");
            
            String temperature = vett[0];
            String intensity = vett[1];
            logger("TEMP "+temperature+" Intensity "+intensity + " (Time: " +cal.get(Calendar.HOUR)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND)+")");
            
        }
        else if(readedString.equals("o")){ //the door is open
            System.out.println(" DOOR OPEN, waiting presence");
            
        }
        
        }
        
    }
    /**Method used to display and log on a file some messages
     * 
     * @param message info to display and save
     */
    public void logger(final String message) {
        //create the writer which will write on the log file
        try (BufferedWriter bw= new BufferedWriter(new FileWriter(file, true))) {
          bw.write(message+"\n"); //write on the file          
          System.out.println(message); //display in the console
        } catch (IOException exception) {
          System.out.println(exception.getMessage());
          exception.printStackTrace();
        }
      }
    


}
